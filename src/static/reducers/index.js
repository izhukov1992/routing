import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import routesReducer from './routes';
import uploaderReducer from './uploader';
import navigationReducer from './navigation';

export default combineReducers({
    routing: routerReducer,
    routes: routesReducer,
    uploader: uploaderReducer,
    navigation: navigationReducer
});
