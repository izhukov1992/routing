import getRandomColor from '../utils';

const initialState = {
    files: []
};

export default function routesReducer(state = initialState, action) {
    switch (action.type) {
        case 'ROUTES_RECEIVE_FULL':
            for (let i = 0; i < action.files.length; i++) {
                action.files[i].visible = false;
                for (let j = 0; j < action.files[i].routes.length; j++) {
                    action.files[i].routes[j].visible = false;
                    action.files[i].routes[j].active = true;
                    action.files[i].routes[j].color = getRandomColor();
                }
            }
            return Object.assign({}, state, {
                files: action.files
            });
        case 'ROUTES_RECEIVE_PARTIAL':
            for (let i = 0; i < action.files.length; i++) {
                action.files[i].visible = false;
                for (let j = 0; j < action.files[i].routes.length; j++) {
                    action.files[i].routes[j].visible = false;
                    action.files[i].routes[j].active = true;
                    action.files[i].routes[j].color = getRandomColor();
                }
            }
            return Object.assign({}, state, {
                files: state.files.concat(action.files)
            });
        case 'ROUTES_DISPLAY_ALL':
            return Object.assign({}, state, {
                ...state,
                files: state.files.map((file, i) => true ? {
                    ...file,
                    visible: true,
                    routes: file.routes.map((route, j) => true ? {
                        ...route,
                        visible: true
                    } : route)
                } : file)
            });
        case 'ROUTES_HIDE_ALL':
            return Object.assign({}, state, {
                ...state,
                files: state.files.map((file, i) => true ? {
                    ...file,
                    visible: false,
                    routes: file.routes.map((route, j) => true ? {
                        ...route,
                        visible: false
                    } : route)
                } : file)
            });
        case 'ROUTES_DISPLAY_FILE_ALL':
            return Object.assign({}, state, {
                ...state,
                files: state.files.map((file, i) => i === action.file ? {
                    ...file,
                    visible: true,
                    routes: file.routes.map((route, j) => true ? {
                        ...route,
                        visible: true
                    } : route)
                } : file)
            });
        case 'ROUTES_HIDE_FILE_ALL':
            return Object.assign({}, state, {
                ...state,
                files: state.files.map((file, i) => i === action.file ? {
                    ...file,
                    visible: false,
                    routes: file.routes.map((route, j) => true ? {
                        ...route,
                        visible: false
                    } : route)
                } : file)
            });
        case 'ROUTES_DEACTIVATE':
            return Object.assign({}, state, {
                ...state,
                files: state.files.map((file, i) => true ? {
                    ...file,
                    routes: file.routes.map((route, j) => true ? {
                        ...route,
                        active: (action.file === i && action.route === j) ? true : false
                    } : route)
                } : file)
            });
        case 'ROUTES_ACTIVATE':
            return Object.assign({}, state, {
                ...state,
                files: state.files.map((file, i) => true ? {
                    ...file,
                    routes: file.routes.map((route, j) => true ? {
                        ...route,
                        active: true
                    } : route)
                } : file)
            });
        default:
            return state;
    }
}