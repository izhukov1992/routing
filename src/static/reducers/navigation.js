const initialState = {
    back: false
};

export default function navigationReducer(state = initialState, action) {
    switch (action.type) {
        case 'SHOW_BACK':
            return Object.assign({}, state, {
                back: true
            });
        case 'HIDE_BACK':
            return Object.assign({}, state, {
                back: false
            });
        default:
            return state;
    }
}