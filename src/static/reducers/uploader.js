const initialState = {
    fileType: undefined,
    fileData: undefined,
    fileName: '',
	visible: false
};

export default function uploaderReducer(state = initialState, action) {
    switch (action.type) {
        case 'UPLOADER_RESET':
            return Object.assign({}, state, {
                fileType: undefined,
                fileData: undefined,
                fileName: ''
            });
        case 'UPLOADER_INIT':
            return Object.assign({}, state, {
                fileName: action.fileName,
                fileType: action.fileType,
                fileData: action.fileData
            });
        case 'UPLOADER_OPEN':
            return Object.assign({}, state, {
                visible: true
            });
        case 'UPLOADER_CLOSE':
            return Object.assign({}, state, {
                visible: false
            });
        default:
            return state;
    }
}