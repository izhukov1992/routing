import React from 'react';
import { Route, Switch } from 'react-router';
import { NotFoundView, MapView } from './containers';

export default(
    <Switch>
        <Route exact path="/" component={MapView} />
        <Route path="*" component={NotFoundView} />
    </Switch>
);
