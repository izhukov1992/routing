import { MapLayer } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet-routing-machine';

export default class Route extends MapLayer {
    constructor(props) {
        super(props)

        this.state = {
            route: null
        }

        this.getWaypoints = this.getWaypoints.bind(this);
    }

    componentDidMount() {}

    hasWaypoints() {
        let has = true;
        const waypoints = this.state.route.getWaypoints();

        for (let i = 0; i < waypoints.length; i++) {
            if (waypoints[i].latLng === null) {
                has = false;
                break;
            }
        }

        return has;
    }

    getWaypoints(route) {
        let waypoints = [];

        for (let i = 0; i < route.stops.length; i++) {
            let stop = route.stops[i];
            waypoints.push(L.latLng(stop.stop_lat, stop.stop_lng));
        }

        return waypoints;
    }

    componentWillUpdate(nextProps, nextState) {
        const { route } = nextProps;

        if (route.visible && route.active) {
            if (!this.hasWaypoints()) {
                this.state.route.setWaypoints(this.getWaypoints(route));
            }
        }
        else {
            this.state.route.setWaypoints([]);
        }
    }

    createLeafletElement(props) {
        const { map, route } = this.props;

        let r = L.Routing.control({
            createMarker: function(waypointIndex, waypoint, numberOfWaypoints) {
                const stop = route.stops[waypointIndex];
                let peoples = 0;
                for (let i = 0; i < waypointIndex; i++) {
                    peoples += route.stops[i].pickup_passengers.length;
                    peoples -= route.stops[i].drop_passengers.length;
                }

                let submarker = '';
                let middle = '';
                if (waypointIndex === 0 || (waypointIndex + 1) === numberOfWaypoints) {
                    if (waypointIndex === 0) {
                        submarker = "<i class='material-icons submarker'>arrow_upward</i>";
                    }
                    else {
                        submarker = "<i class='material-icons submarker'>arrow_downward</i>";
                    }
                }
                else {
                  middle = 'stop';
                }

                let m = L.marker(waypoint.latLng, {
                  icon: L.divIcon({
                    html: '<div class="marker ' + middle + '" style="background: ' + route.color + '">' + submarker + '</div>',
                    className: 'markericon'
                  })
                }).bindPopup('\
                    <table>\
                        <tbody>\
                            <tr>\
                                <th># of people on board before drop and pickup</th>\
                                <th>' + peoples + '</th>\
                            </tr>\
                            <tr>\
                                <td># of drop passangers</td>\
                                <td>' + stop.drop_passengers.length + '</td>\
                            </tr>\
                            <tr>\
                                <td># of pickup passangers</td>\
                                <td>' + stop.pickup_passengers.length + '</td>\
                            </tr>\
                        </tbody>\
                    </table>');

                m.on('mouseover', function (e) {
                    this.openPopup();
                });
                m.on('mouseout', function (e) {
                    this.closePopup();
                });

                return m;
            },
            position: 'bottomright',
            waypoints: [],
            /* Change OSRM-server URL to any another 
            router: new L.Routing.OSRMv1({
                serviceUrl: 'http://ec2-52-34-14-25.us-west-2.compute.amazonaws.com:5000/route/v1'
            }),*/
            lineOptions: {
                styles: [
                    {color: 'black', opacity: 0.15, weight: 9},
                    {color: 'white', opacity: 0.8, weight: 6},
                    {color: route.color, opacity: 1, weight: 2}
                ],
            },
            routeWhileDragging: false,
            addWaypoints: false,
            draggableWaypoints: false,
            routeLine: function(r, options) {
                let line = L.Routing.line(r, options);
                line.eachLayer(function(l) {
                    function handleHoverRoute(e) {
                        let approx = [];
                        for (var i = 0; i < l._latlngs.length; i++) {
                            const c = l._latlngs[i];
                            if (r.waypointIndices.indexOf(i - 1) > -1 && i - 1 > 0) {
                                const lat = (l._latlngs[i + 1].lat - c.lat) / 5 + c.lat;
                                const lng = (l._latlngs[i + 1].lng - c.lng) / 5 + c.lng;
                                approx.push(Math.abs((e.latlng.lat - lat) * (e.latlng.lng - lng)));
                            }
                            else {
                                approx.push(Math.abs((e.latlng.lat - c.lat) * (e.latlng.lng - c.lng)));
                            }
                        }

                        const coordinatesIndex = approx.indexOf(Math.min(...approx));

                        let waypointIndex = 0;
                        for (var i = 0; i < r.waypointIndices.length; i++) {
                            waypointIndex = i;
                            if (r.waypointIndices[i] >= coordinatesIndex) {
                                break;
                            }
                        }

                        let peoples = 0;
                        for (var i = 0; i < waypointIndex; i++) {
                            peoples += route.stops[i].pickup_passengers.length;
                            peoples -= route.stops[i].drop_passengers.length;
                        }

                        const stop = route.stops[waypointIndex];
                        l.bindPopup('\
                            <table>\
                                <tbody>\
                                    <tr>\
                                        <th># of people on board</th>\
                                        <th>' + peoples + '</th>\
                                    </tr>\
                                    <tr>\
                                        <td># of drop passangers next stop</td>\
                                        <td>' + stop.drop_passengers.length + '</td>\
                                    </tr>\
                                    <tr>\
                                        <td># of pickup passangers next stop</td>\
                                        <td>' + stop.pickup_passengers.length + '</td>\
                                    </tr>\
                                </tbody>\
                            </table>');
                        l.openPopup(e.latlng);
                    }

                    l.on('mouseover', handleHoverRoute);
                    l.on('mouseout', function (e) {
                        this.closePopup();
                    });
                });
                return line;
            },
        }).addTo(map.leafletElement);

        this.setState({route: r});
    }
}
