import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    Button,
    DialogContainer,
    CardActions,
    FileUpload,
    Snackbar,
    TextField
} from 'react-md';

import { sendRoutes, initUploader, resetUploader, openUploader, closeUploader } from '../../actions';

class Uploader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            toasts: []
        };

        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleLoad = this.handleLoad.bind(this);
        this.addToast = this.addToast.bind(this);
        this.dismiss = this.dismiss.bind(this);
    }

    show = () => {
        this.props.dispatch(openUploader());
    }

    hide = () => {
        this.props.dispatch(closeUploader());
        this.props.dispatch(resetUploader());
    }

    handleSubmit = (e) => {
        e.preventDefault();

        if (this.props.uploader.fileType === undefined) {
            this.addToast('A file is required.');
            return;
        }
        else if (this.props.uploader.fileType !== 'application/json') {
            this.addToast('Wrong file type.');
            return;
        }

        this.props.sendRoutes();
    }

    handleLoad = ({ name, size, type }, data) => {
        this.props.dispatch(initUploader(name, type, data));
    }

    addToast = (text) => {
        const toasts = [{ text, action: 'Ok' }];
        this.setState({ toasts });
    }

    dismiss = () => {
        const [, ...toasts] = this.state.toasts;
        this.setState({ toasts });
    }

    render() {
        return (
            <div>
                <div className="map-controls uploader" hidden={this.props.back}>
                    <Button className="md-btn-default" floating onClick={this.show}>file_upload</Button>
                </div>
                <DialogContainer
                  id="simple-action-dialog"
                  visible={this.props.visible}
                  onHide={this.hide}
                  title="Upload routes"
                >
                    <form
                        id="server-upload-form"
                        ref={this.setForm}
                        onSubmit={this.handleSubmit}
                        name="server-upload-form"
                    >
                        <FileUpload
                            id="server-upload-file"
                            label="Choose file"
                            accept="application/json"
                            onLoad={this.handleLoad}
                            name="file"
                            iconBefore
                            primary
                            required
                        />
                        <TextField
                            id="server-upload-file-field"
                            placeholder="No file chosen"
                            value={this.props.uploader.fileName}
                            fullWidth={false}
                            readOnly
                        />
                        <CardActions className="md-full-width">
                            <Button type="submit" className="md-cell--right" flat primary disabled={this.props.uploader.fileType !== 'application/json'}>Submit</Button>
                        </CardActions>
                    </form>
                </DialogContainer>
            <Snackbar id="file-upload-errors" toasts={this.state.toasts} onDismiss={this.dismiss} />
          </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        uploader: state.uploader,
		back: state.navigation.back
    };
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    sendRoutes: bindActionCreators(sendRoutes, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Uploader);
export { Uploader as UploaderNotConnected };
