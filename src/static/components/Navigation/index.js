import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Button, Drawer, Toolbar, List, ListItem, ExpansionList, ExpansionPanel } from 'react-md';

import { getRoutes, displayRoutes, hideRoutes, displayFileRoutes, hideFileRoutes, deactivateRoutes, activateRoutes, showBack, hideBack } from '../../actions';
import {  } from '../../actions';

class Navigation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fullscreen: false,
            visible: false,
            position: 'right'
        };

        this.handleRouteClick = this.handleRouteClick.bind(this);
        this.openDrawerRight = this.openDrawerRight.bind(this);
        this.closeDrawer = this.closeDrawer.bind(this);
        this.handleVisibility = this.handleVisibility.bind(this);
        this.handleFullScreen = this.handleFullScreen.bind(this);
        this.toggleFullScreen = this.toggleFullScreen.bind(this);
        this.showAllRoutes = this.showAllRoutes.bind(this);
        this.hideAllRoutes = this.hideAllRoutes.bind(this);
        this.activateAllRoutes = this.activateAllRoutes.bind(this);
        this.handleExpand = this.handleExpand.bind(this);
    }

    componentWillMount() {
        this.props.getRoutes();
    }

    componentDidMount() {
        document.addEventListener("fullscreenchange", this.handleFullScreen, false);
        document.addEventListener("mozfullscreenchange", this.handleFullScreen, false);
        document.addEventListener("webkitfullscreenchange", this.handleFullScreen, false);
        document.addEventListener("msfullscreenchange", this.handleFullScreen, false);
    }

    componentWillUnmount() {
        document.removeEventListener("fullscreenchange", this.handleFullScreen, false);
        document.removeEventListener("mozfullscreenchange", this.handleFullScreen, false);
        document.removeEventListener("webkitfullscreenchange", this.handleFullScreen, false);
        document.removeEventListener("msfullscreenchange", this.handleFullScreen, false);
    }

    handleRouteClick = (i, j) => {
        this.closeDrawer();
		this.props.dispatch(deactivateRoutes(i, j));
		this.props.dispatch(showBack());
    }

    openDrawerRight = () => {
        this.setState({ visible: true });
    }

    closeDrawer = () => {
        this.setState({ visible: false });
    }

    handleVisibility = (visible) => {
        this.setState({ visible });
    }

    handleFullScreen = () => {
        this.setState({fullscreen: !this.state.fullscreen});
    }

    toggleFullScreen = () => {
        if (document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement) {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            }
            else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            }
            else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
        else {
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            }
            else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            }
            else if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        }
    }

    showAllRoutes = () => {
		this.props.dispatch(displayRoutes());
    }

    hideAllRoutes = () => {
		this.props.dispatch(hideRoutes());
    }

    activateAllRoutes = () => {
		this.props.dispatch(activateRoutes());
		this.props.dispatch(hideBack());
    }

    handleExpand = (expanded, file) => {
        if (expanded) {
            this.props.dispatch(displayFileRoutes(file));
        }
        else {
            this.props.dispatch(hideFileRoutes(file));
        }
    }

    render() {
        const closeBtn = <Button icon onClick={this.closeDrawer}>close</Button>;
        const allRoutesBtn = <Button className="md-btn-default" floating onClick={this.showAllRoutes}>location_on</Button>;
        const noneRoutesBtn = <Button className="md-btn-default" floating onClick={this.hideAllRoutes}>location_off</Button>;
        const activateRoutesBtn = <Button className="md-btn-default" floating onClick={this.activateAllRoutes}>arrow_back</Button>;
        const fullScrnBtn = this.state.fullscreen ?
            <Button className="md-btn-default" floating onClick={this.toggleFullScreen}>fullscreen_exit</Button> :
            <Button className="md-btn-default" floating onClick={this.toggleFullScreen}>fullscreen</Button>;
            
        return (
            <div>
                <div className="map-controls" hidden={!this.props.back}>
                    {activateRoutesBtn}
                </div>
                <div className="map-controls" hidden={this.props.back}>
                    <Button className="md-btn-default" floating onClick={this.openDrawerRight}>menu</Button>
                </div>
                <div className="map-controls" hidden={this.props.back} style={{top: 152}}>
                    {fullScrnBtn}
                </div>
                <div className="map-controls" hidden={this.props.back} style={{top: 222}}>
                    {allRoutesBtn}
                </div>
                <div className="map-controls" hidden={this.props.back} style={{top: 292}}>
                    {noneRoutesBtn}
                </div>
                <Drawer
                    id="simple-drawer-example"
                    type={Drawer.DrawerTypes.TEMPORARY}
                    visible={this.state.visible}
                    position={this.state.position}
                    onVisibilityChange={this.handleVisibility}
                >
                    <Toolbar
                        title="Routes"
                        actions={closeBtn}
                        className="md-divider-border md-divider-border--bottom"
                    />
                    <ExpansionList className="drawers__content__scrollable">
                        {this.props.files &&
                         this.props.files.map((file, i) => (
                            <ExpansionPanel key={i} label={file.filename + " " + (new Date(file.date)).toLocaleString()} footer={null} onExpandToggle={(expanded) => this.handleExpand(expanded, i)} defaultExpanded={file.visible}>
                                <List>
                                    {file.routes &&
                                     file.routes.map((route, j) => <ListItem key={j} primaryText={route.vehicle} onClick={() => this.handleRouteClick(i, j)}><div className="route-marker" style={{background: route.color}}></div></ListItem>)}
                                </List>
                            </ExpansionPanel>)
                         )}
                    </ExpansionList>
                </Drawer>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        files: state.routes.files,
		back: state.navigation.back
    };
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    getRoutes: bindActionCreators(getRoutes, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
export { Navigation as NavigationNotConnected };
