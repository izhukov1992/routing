export function receiveRoutesFull(files) {
    return {
        type: 'ROUTES_RECEIVE_FULL',
        files
    };
}

export function receiveRoutesPartial(files) {
    return {
        type: 'ROUTES_RECEIVE_PARTIAL',
        files
    };
}

export function displayRoutes() {
    return {
        type: 'ROUTES_DISPLAY_ALL'
    };
}

export function hideRoutes() {
    return {
        type: 'ROUTES_HIDE_ALL'
    };
}

export function displayFileRoutes(file) {
    return {
        type: 'ROUTES_DISPLAY_FILE_ALL',
        file
    };
}

export function hideFileRoutes(file) {
    return {
        type: 'ROUTES_HIDE_FILE_ALL',
        file
    };
}

export function deactivateRoutes(file, route) {
    return {
        type: 'ROUTES_DEACTIVATE',
        file,
        route
    };
}

export function activateRoutes() {
    return {
        type: 'ROUTES_ACTIVATE'
    };
}

export function initUploader(fileName, fileType, fileData) {
    return {
        type: 'UPLOADER_INIT',
        fileName,
        fileType,
        fileData
    };
}

export function resetUploader() {
    return {
        type: 'UPLOADER_RESET'
    };
}

export function openUploader() {
    return {
        type: 'UPLOADER_OPEN'
    };
}

export function closeUploader() {
    return {
        type: 'UPLOADER_CLOSE'
    };
}

export function showBack() {
    return {
        type: 'SHOW_BACK'
    };
}

export function hideBack() {
    return {
        type: 'HIDE_BACK'
    };
}

export function sendRoutes() {
    return (dispatch, getState) => {
        const { uploader } = getState();

        fetch('api/v1/routing/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                filename: uploader.fileName,
                text: uploader.fileData
            })
        }).then((response) => {
            if (response.ok) {
                response.json().then(json => {
                    dispatch(receiveRoutesPartial(json.files));
                });

				dispatch(closeUploader());
                dispatch(resetUploader());
            }
            else {
                const error = new Error(response.statusText);
                error.response = response;
                throw error;
            }
        }).catch((error) => {
            // this.addToast(error.message); TODO action to show toast
        });
    }
}

export function getRoutes() {
    return (dispatch) => {
        fetch('api/v1/routing/', {
            method: 'GET'
        }).then((response) => {
            if (response.ok) {
                response.json().then(json => {
                    dispatch(receiveRoutesFull(json.files));
                })
            }
            else {
                const error = new Error(response.statusText);
                error.response = response;
                throw error;
            }
        }).catch((error) => {
            // this.addToast(error.message); TODO action to show toast
        });
    }
}