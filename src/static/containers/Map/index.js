import React from 'react';
import { connect } from 'react-redux';
import { Map, TileLayer, Marker, Popup, LayersControl, ZoomControl } from 'react-leaflet';
import { GoogleLayer } from 'react-leaflet-google';
const { BaseLayer} = LayersControl;

import { API_KEY, TERRAIN, ROADMAP, SATELLITE, HYBRID } from '../../constants';

import Route from '../../components/Route';
import Navigation from '../../components/Navigation';
import Uploader from '../../components/Uploader';

import 'react-md/dist/react-md.green-cyan.min.css';
import 'leaflet/dist/leaflet.css';
import 'leaflet-routing-machine/dist/leaflet-routing-machine.css'
import './style.scss';

class MapView extends React.Component {
    render() {
        return (
            <div id="map">
                <Navigation />
                <Uploader visible={this.props.uploader.visible} />
                <div className="map-frame">
                    <Map center={[12.59, 77.35]} zoom={13} zoomControl={false} attributionControl={false} ref='map'>
                        <LayersControl position='topleft'>
                            <BaseLayer name='OpenStreetMap.Mapnik'>
                                <TileLayer url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"/>
                            </BaseLayer>
                            <BaseLayer checked name='Google Maps Roads'>
                                <GoogleLayer googlekey={API_KEY} maptype={ROADMAP} />
                            </BaseLayer>
                            <BaseLayer name='Google Maps Terrain'>
                                <GoogleLayer googlekey={API_KEY} maptype={TERRAIN} />
                            </BaseLayer>
                            <BaseLayer name='Google Maps Satellite'>
                                <GoogleLayer googlekey={API_KEY} maptype={SATELLITE} />
                            </BaseLayer>
                            <BaseLayer name='Google Maps Hydrid'>
                                <GoogleLayer googlekey={API_KEY} maptype={HYBRID} libraries={['geometry', 'places']} />
                            </BaseLayer>
                            <BaseLayer name='Google Maps with Libraries'>
                                <GoogleLayer googlekey={API_KEY} maptype={HYBRID} libraries={['geometry', 'places']} />
                            </BaseLayer>
                        </LayersControl>
                        <ZoomControl position='bottomleft' />
                        {this.props.files &&
                         this.props.files.map((file, i) => (
                            file.routes &&
                            file.routes.map((route, j) => <Route key={"" + i + j} map={this.refs.map} route={route} />)
                        ))}
                    </Map>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        files: state.routes.files,
        uploader: state.uploader
    };
}

export default connect(mapStateToProps)(MapView);
export { MapView as MapViewNotConnected };
