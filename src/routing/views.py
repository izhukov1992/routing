from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

import json

from routing.models import RoutesFile, Route


class RouteView(APIView):
    """Route model viewset"""

    def post(self, request):
        text = request.data.get('text')
        filename = request.data.get('filename')

        response_data = {'error': 'Bad data'}
        response_status = status.HTTP_400_BAD_REQUEST

        try:
            data = json.loads(text)

            routes = data.get('routes')

            if routes:
                routes_file = RoutesFile.objects.create(filename=filename)

                for route in routes:
                    Route.objects.create(routes_file=routes_file, text=json.dumps(route))

                routes_files = []
                routes_files.append({
                    'date': routes_file.date,
                    'filename': routes_file.filename,
                    'routes': [json.loads(route.text) for route in routes_file.routes.all()]
                })
                response_data = {'files': routes_files}
                response_status = status.HTTP_201_CREATED

        except:
            pass

        return Response(response_data, status=response_status)

    def get(self, request):
        routes_files = []

        for routes_file in RoutesFile.objects.all():
            routes_files.append({
                'date': routes_file.date,
                'filename': routes_file.filename,
                'routes': [json.loads(route.text) for route in routes_file.routes.all()]
            })

        return JsonResponse({'files': routes_files})
