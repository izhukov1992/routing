from django.db import models


class RoutesFile(models.Model):
    """RoutesFile class model."""

    date = models.DateTimeField(auto_now=True)
    filename = models.CharField(max_length=255)


class Route(models.Model):
    """Route class model."""

    routes_file = models.ForeignKey(RoutesFile, related_name='routes')
    text = models.TextField()
