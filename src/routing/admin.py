from django.contrib import admin

from routing.models import Route, RoutesFile


admin.site.register(Route, admin.ModelAdmin)
admin.site.register(RoutesFile, admin.ModelAdmin)
