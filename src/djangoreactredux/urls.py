from django.conf import settings
from django.conf.urls import include, url
from django.views.decorators.cache import cache_page
from django.contrib import admin
from rest_framework import routers

from base import views as base_views
from routing.views import RouteView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/routing/', RouteView.as_view()),

    # catch all others because of how history is handled by react router - cache this page because it will never change
    url(r'', cache_page(settings.PAGE_CACHE_SECONDS)(base_views.IndexView.as_view()), name='index'),
]
